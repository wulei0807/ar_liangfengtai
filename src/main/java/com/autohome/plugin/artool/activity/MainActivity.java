package com.autohome.plugin.artool.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.autohome.plugin.artool.R;


/**
 * Created by weiss on 2017/4/20.
 */

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    public void startARM4(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("autohome://ar/main?seriesid=4094&from=m4from&pvclickurl=m4url&backscheme=m4back")));
    }

    public void startARGS8(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("autohome://ar/main?seriesid=3691&from=gs8from&pvclickurl=gs8url&backscheme=gs8back")));
    }

    public void startQR(View view) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("autohome://ar/scan")));
    }
}
