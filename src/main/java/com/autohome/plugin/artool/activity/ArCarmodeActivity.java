package com.autohome.plugin.artool.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;

import com.autohome.mainlib.business.analysis.UmsAnalytics;
import com.autohome.mainlib.business.analysis.UmsParams;
import com.hiscene.sdk.Constants;
import com.hiscene.sdk.R;
import com.hiscene.sdk.fragment.BaseARFragment;
import com.hiscene.sdk.listener.InteractiveListener;
import com.hiscene.sdk.statistics.ParameterHelper;
import com.hiscene.sdk.statistics.StatisticsHelper;
import com.hiscene.sdk.utils.UtilsManager;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.connection.FileDownloadUrlConnection;
import com.liulishuo.filedownloader.services.DownloadMgrInitialParams;

import java.net.Proxy;

/**
 * Created by li on 2017/4/16.
 */

public class ArCarmodeActivity extends BaseActivity implements InteractiveListener {
    private BaseARFragment arFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar);
        //辅助工具初始化
        UtilsManager.init(this);
        /**
         * just for cache Application's Context, and ':filedownloader' progress will NOT be launched
         * by below code, so please do not worry about performance.
         * @see FileDownloader#init(Context)
         */
        FileDownloader.init(new DownloadMgrInitialParams.InitCustomMaker()
                .connectionCreator(new FileDownloadUrlConnection
                        .Creator(new FileDownloadUrlConnection.Configuration()
                        .connectTimeout(15_000) // set connection timeout.
                        .readTimeout(15_000) // set read timeout.
                        .proxy(Proxy.NO_PROXY) // set proxy
                )));
        matcher();
        arFragment = new BaseARFragment();
        Bundle bundle = new Bundle();
        bundle.putString("CarName", seriesid);
        bundle.putInt("Type", Constants.GameType.ModelGame.ordinal());
        arFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_ar, arFragment).commit();

        UmsAnalytics.pvBegin(StatisticsHelper.AR_SHOWCAR_RESIDENCE_TIME_PV, new UmsParams());
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP:
                arFragment.setActionUpTime(System.currentTimeMillis());
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        arFragment.onBackPressed();
        super.onBackPressed();
    }

    @Override
    public void startWebActivity(String url) {
        if (url == null) return;
        Intent itt = new Intent(ArCarmodeActivity.this, ViewWebActivity.class);
        itt.setData(Uri.parse(url));
        startActivity(itt);
    }

    private String seriesid;

    private void matcher() {
        Uri uri = getIntent().getData();
        seriesid = uri.getQueryParameter("seriesid");//车系ID
        ParameterHelper.getInstance().setSeriesId(checkNull(seriesid));
        String from = uri.getQueryParameter("from");//点击入口来源
        ParameterHelper.getInstance().setFrom(checkNull(from));
        String pvclickurl = uri.getQueryParameter("pvclickurl");//广告位点击上报地址
        ParameterHelper.getInstance().setPvclickurl(checkNull(pvclickurl));
        String backscheme = uri.getQueryParameter("backscheme");//后退时打开指定页面的scheme
        ParameterHelper.getInstance().setBackscheme(checkNull(backscheme));
        Log.i("matcher", " seriesid: " + seriesid + " from: " + from + " pvclickurl: " + pvclickurl + " backscheme: " + backscheme);
    }

    /**
     * 判断字符串是否为null
     *
     * @param str 待校验字符串
     * @return {@code true}: "" Or <br> {@code false}: str
     */
    public static String checkNull(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return str;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UmsAnalytics.pvEnd(StatisticsHelper.AR_SHOWCAR_RESIDENCE_TIME_PV);
    }
}
