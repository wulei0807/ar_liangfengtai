package com.autohome.plugin.artool.activity;

import android.content.Context;
import android.os.Bundle;

import com.hiscene.sdk.Constants;
import com.hiscene.sdk.fragment.BaseQRFragment;
import com.hiscene.sdk.utils.UtilsManager;
import com.liulishuo.filedownloader.FileDownloader;
import com.liulishuo.filedownloader.connection.FileDownloadUrlConnection;
import com.liulishuo.filedownloader.services.DownloadMgrInitialParams;

import java.net.Proxy;

/**
 * Created by li on 2017/4/24.
 */

public class ArScanRedPacketActivity extends BaseActivity {
    private BaseQRFragment qrFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.hiscene.sdk.R.layout.activity_ar);

        //辅助工具初始化
        UtilsManager.init(this);
        /**
         * just for cache Application's Context, and ':filedownloader' progress will NOT be launched
         * by below code, so please do not worry about performance.
         * @see FileDownloader#init(Context)
         */
        FileDownloader.init(new DownloadMgrInitialParams.InitCustomMaker()
                .connectionCreator(new FileDownloadUrlConnection
                        .Creator(new FileDownloadUrlConnection.Configuration()
                        .connectTimeout(15_000) // set connection timeout.
                        .readTimeout(15_000) // set read timeout.
                        .proxy(Proxy.NO_PROXY) // set proxy
                )));
//        String path = FileUtil.getDiskCacheDir(this) + File.separator;
//        FileUtil.copyAssetsFolder2SDCard(this, "games", path, true);
//        Global.setResourcePath(path + "demo_car" + File.separator);
        String name="demo_redenvelope";
        qrFragment = new BaseQRFragment();
        Bundle bundle = new Bundle();
        bundle.putString("CarName", name);
        bundle.putInt("Type", Constants.GameType.RedBagGame.ordinal());
        qrFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(com.hiscene.sdk.R.id.fragment_ar, qrFragment).commit();
    }
}
