package com.autohome.plugin.artool.activity;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.autohome.mainlib.core.AHBaseApplication;
import com.autohome.net.AHNetConfigs;
import com.hiscene.sdk.utils.UtilsManager;

/**
 * Created by li on 2017/4/24.
 *
 * 此类作为功能使用，集成主APP中此类所有修改无效
 */

public class ARApplication extends AHBaseApplication {

    public void onCreate() {
        super.onCreate();
        // 设置是否开启HttpDNS
        AHNetConfigs.getInstance().setHttpDNSEnable(false);
        // 设置是否开启反向代理
        AHNetConfigs.getInstance().setReverseProxyEnable(false);
        // 设置是否开启反劫持
        AHNetConfigs.getInstance().setAntiHijackEnable(false);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

}
